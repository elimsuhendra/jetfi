<?php

namespace digipos\Libraries;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class kyubi
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *  
     * @param  updateambulanceadmin  $event
     * @return void
     */
    public static function getDateIntervalToYearMontDay($dt1, $dt2)
    {
        $date1          = date_create(date_format(date_create($dt1),'Y-m-d'));
        $date2          = date_create(date_format(date_create($dt2),'Y-m-d'));
        $diff           = date_diff($date1,$date2);
        $interval_day   = $diff->format("%a");
        $y              = (int)($interval_day / 360);
        $m              = (int)(($interval_day - ($y * 360)) / 30);
        $d              = (int)(($interval_day - ($y * 360) - ($m * 30)));

        return $arr = [
            "year"  => $y,
            "month" => $m,
            "day" => $d,
            "interval_day" => $interval_day
        ];
    }

    public static function rupiah($angka){
    
        $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
        return $hasil_rupiah;

    }

     public static function decode_rupiah($price){
        if($price != 'Nan'){
            $price = str_replace(',', '', $price);
            $price = substr($price, 0, strpos($price, '.'));
            return $price;
        }else{
             return null;
        }
    }

    /*
        fucntion check string consist of alphabet and numeric
    */
    public static function check_alphanumeric($string){
        $alpha      = ctype_alpha($string);
        $num2       = is_numeric($string);
        // $string1 = "asd 123";
        // $string2 = "this is a string";
        // $regex      = preg_match('/[^a-z_\-0-9]/i', $string1);
        // $regex2      = preg_match('/[^a-z_\-0-9]/i', $string2);
        // $regex3      = preg_match( '/[^A-Za-z0-9]+/', '123@asd');
        // dd($alpha.'-'.$num.'-'.$num2.'-'.$regex.'-'.$regex2.'-'.$regex3);
        if($alpha == '' && $num2 == ''){
                return true;
        }else{
            return false;
        }
    }

     /*
        fucntion compare array, and get array different from new array or $arr1 from compare array or $arr2
    */
    public static function array_different($arr1, $arr2){
        $arr1 = [5,6,7];
        $arr2 = [5,6];
        $res = []; 
        
        foreach ($arr1 as $v) {
            if(!in_array($v, $arr2)){
                array_push($res, $v);
            }
        }
        var_dump($res);
        if($res){
                return $res;
        }else{
            return false;
        }
    }
}
