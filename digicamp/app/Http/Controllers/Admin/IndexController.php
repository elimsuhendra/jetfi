<?php namespace digipos\Http\Controllers\Admin;
use digipos\Libraries\Email;

use digipos\models\Province;
use digipos\models\City;
use digipos\models\Order_hd;
use digipos\models\Order_dt;

use DB;

class IndexController extends Controller {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard); 
		$this->data['title'] 	= "Dashboard";
		// $this->merchant 		= new Msmerchant;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		// $this->data['top_order_success']				=	Mitra::select('mitra.*', DB::raw('COUNT(*) as mitra_count'))->join('orderdt', 'mitra.id', 'orderdt.mitra_id')->where('orderdt.status_order', '6')->groupBy('mitra.name')->orderBy('mitra_count', 'desc')->limit(5)->get();
		// // dd($this->data['top_order_success']);
		// $this->data['top_mitra_rating']					=	Mitra::select('mitra.*', 'mitradt.total_rating', 'mitradt.total_user_rating')->join('mitradt','mitra.id','mitradt.mitra_id')->orderBy('mitradt.total_rating', 'desc')->orderBy('mitradt.total_user_rating','desc')->limit(5)->get();
		// // dd($this->data['top_mitra_rating']);

		// $this->data['top_customer_order_success']		= Customer::select('customer.*', 'orderhd.customer_id', DB::raw('COUNT(*) as count'))->join('orderhd','orderhd.customer_id','customer.id')->where('order_status', '6')->groupBy('customer.id')->get();
		// // dd($this->data['top_customer_order_success']);
		
		$country_id = 1;
		$order_success				=	Order_hd::select('orderhd.*')->leftjoin('orderdt', 'orderhd.id', 'orderdt.orderhd_id')->where('orderhd.order_status_id', '6')->limit(5)->get();

		$arr_temp = [];
		foreach ($order_success as $key => $val) {
			$country = json_decode($val->country_id);

			if(count($country) > 0){
				foreach ($country as $key2 => $val2) {
					// var_dump(count($arr_temp));
					if(count($arr_temp) > 0){
						$flagFound = 0;
						$j = 0;
						// var_dump($arr_temp);
						foreach ($arr_temp as $key3 => $val3) {
							if($val3['country_id'] == $val2){
								// var_dump($val3['country_id'].' - '.$val3['count'].' --'.$val2);
								$arr_temp[$j]['count'] += 1;
								$flagFound = 1;
								// var_dump($val3);
								// var_dump($arr_temp);
							}
							$j++;
						}

						if($flagFound == 0){
							$country = DB::table('country')->find($val2);
							$temp_arr = [
								"country_id" => $val2,
								"country_name" => $country->country_name,
								"count" => 1
							];
							array_push($arr_temp, $temp_arr);
							// $arr_temp = asort($arr_temp);
						}
					}else{
						$country = DB::table('country')->find($val2);
						$temp_arr = [
							"country_id" => $val2,
							"country_name" => $country->country_name,
							"count" => 1
						];
						array_push($arr_temp, $temp_arr);
						// $arr_temp = asort($arr_temp);
					}
					
				}
			}
		}
		// dd($arr_temp);
		$this->data['top_country_order_success'] = $arr_temp;
		// dd($this->data['top_country_order_success']);

		// check modem rent pass day
		$this->checkOrderPassDay();
		return $this->render_view('pages.index');
	}

}
