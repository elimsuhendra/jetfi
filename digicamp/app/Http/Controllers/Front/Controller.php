<?php namespace digipos\Http\Controllers\Front;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Request;
use View;
use Cache;
use Cookie;

use digipos\models\Config;
use digipos\models\Social_media;
class Controller extends BaseController {

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public $data         = [];
    public $root_path    = '/';
    public $view_path    = 'front';
    public $guard        = 'web';
    public $auth_guard   = 'auth:web';
    public $guest_guard  = 'guest:web';

    public function __construct() {
        $this->data['guard']        = $this->guard;
        $this->data['root_path']    = $this->root_path;
        $this->data['canonical']    = $this->root_path;
        $this->data['view_path']    = $this->view_path;
        $this->data['meta_robots']  = 'index';
        //Global Number
        $no = 1;
        if (Request::has('page')){
            $no = Request::input('page') * 10 - 9;
        }
        $this->data['no']                   = $no;
        $this->data['my_ip']                = $_SERVER['REMOTE_ADDR'];
        $this->data['web_name']             = Config::where('name','web_name')->first()->value;
        $this->data['web_logo']             = Config::where('name','web_logo')->first()->value;
        $this->data['web_email']            = Config::where('name','web_email')->first()->value;
        $this->data['web_title']            = Config::where('name','web_title')->first()->value;
        $this->data['web_description']      = Config::where('name','web_description')->first()->value;
        $this->data['web_keywords']         = Config::where('name','web_keywords')->first()->value;
        $this->data['favicon']              = Config::where('name','favicon')->first()->value;
        $this->data['meta']                 = ['title' => 'Pos Indonesia', 'keywords' => 'Pos Indonesia', 'description' => 'Pos Indonesia'];
        // $this->data['social_media']          = Social_media::get();
        // dd($this->data['meta']);

        // $path = explode('/',Request::path());
        $this->data['path'] = $this->root_path;

        $path = explode('/',Request::path());
        // dd($path);
        $this->data['path'] = $path;
        $this->data['path2'] = $path;
        // dd($this->data['path']);
    }

    public function render_view($view = ''){
        $data = $this->data;
        // if($view == "pages.pages.cek_alamat"){
        //     return view($this->view_path.'.'.$view, [
        //         'cek_alamat' => $data->appends(Input::except('page'))
        //     ]);
       
        // }else{
        //     return view($this->view_path.'.'.$view,$data);       
        // }
        // dd($data);
        return view($this->view_path.'.'.$view,$data);  
    }
}
