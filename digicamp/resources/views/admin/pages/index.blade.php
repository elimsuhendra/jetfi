@extends($view_path.'.layouts.master')
@section('content')
@stack('scripts')
<script src="{{asset('components/plugins/amcharts/amcharts/amcharts.js')}}"></script>
<script src="{{asset('components/plugins/amcharts/amcharts/serial.js')}}"></script>
<!-- Load the JavaScript API client and Sign-in library. -->
<script src="https://apis.google.com/js/client:platform.js"></script>
@push('styles')

@endpush
<!-- <div class="row">
	<div class="col-md-12 dashboard coming-soon">
		<img class="img-responsive" src="{{asset('components/back/images/admin/dashboard.jpg')}}">
	</div>
</div> -->

	<div class="row">
      	<div class="col-md-6 col-sm-6">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-share font-blue"></i>
                        <span class="caption-subject font-blue bold uppercase">Top Country By Order Success</span>
                    </div>
                    <div class="actions">
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="scroller" style="height: 300px;" data-always-visible="1" data-rail-visible="0">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th> # </th>
                                        <th> Name </th>
                                        <th> Total Order Success</th>
                                    </tr>
                                    @if(count($top_country_order_success) > 0)
                                        @php 
                                            $i=1;
                                        @endphp
                                        @foreach($top_country_order_success as $val)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td>{{$val['country_name']}}</td>
                                                <td>{{$val['count']}}</td>
                                            </tr>
                                            @php 
                                                $i++;
                                            @endphp
                                        @endforeach
                                    @endif
                                </thead>
                                <tbody>
                                	
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- END ROW -->

@push('scripts')
<script type="text/javascript">
	$(document).ready(function(){
        
    });
</script>
@endpush
@endsection
